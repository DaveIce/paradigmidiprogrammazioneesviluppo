package u04lab.code

import List.*

trait Student:
  def name: String
  def year: Int
  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?

trait Course:
  def name: String
  def teacher: String

object Student:
  private case class StudentImpl(override val name: String, override val year: Int) extends Student:
    var coursesList: List[Course] = Nil()
    override def enrolling(c: Course*) = c.foreach(course=>coursesList = append(coursesList, Cons(course, Nil())))
    override def hasTeacher(teacher: String): Boolean = contains(courses, teacher)
    override def courses: List[String] = map(coursesList)((c: Course) => c.teacher)

  def apply(name: String, year: Int = 2017): Student = new StudentImpl(name, year)

object Course:
  private case class CourseImpl(override val name: String, override val teacher: String) extends Course
  def apply(name: String, teacher: String): Course = new CourseImpl(name, teacher)

object SameTeacher:
  def unapply(l: List[Course]): scala.Option[String] = l match
    case Cons(h, t) => if length(filter(l)(_.teacher != h.teacher)) == 0 then Some(h.teacher) else None


@main def checkStudents(): Unit =
  val cPPS = Course("PPS", "Ricci")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "Ricci")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") // defaults to 2017
  s1.enrolling(cPPS, cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(
    (s1.courses, s2.courses, s3.courses)
  ) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true

  val courses = List(cPPS ,cPCD ,cSDR)
  courses match
    case SameTeacher(t) => println(s"$courses have same teacher $t")
    case _ => println(s"$courses have different teachers")


/** Hints:
  *   - simply implement Course, e.g. with a case class
  *   - implement Student with a StudentImpl keeping a private Set of courses
  *   - try to implement in StudentImpl method courses with map
  *   - try to implement in StudentImpl method hasTeacher with map and find
  *   - check that the two println above work correctly
  *   - refactor the code so that method enrolling accepts a variable argument Course*
  */
