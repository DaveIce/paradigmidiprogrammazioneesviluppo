package u02

object Es5 extends App:
  def compose(f: Int => Int, g: Int => Int): Int => Int = i => f(g(i))

  println(compose(_ - 1, _ * 2)(5))

  def genericCompose[A, B, C](f: B => C, g: A => B): A => C = i => f(g(i))

  println(genericCompose((n: Int) => n + "toString", (n: Int) => n * 2)(5))
