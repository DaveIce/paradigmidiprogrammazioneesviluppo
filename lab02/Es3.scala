package u02

// Exercise 3
object Es3 extends App :

  def parity(x: Int): String = x match
    case x if x % 2 == 0 => "even"
    case _ => "odd"

  val valParity: Int => String = _ match
    case x if x % 2 == 0 => "even"
    case _ => "odd"

  def neg(fun: String => Boolean): String => Boolean = (x: String) => !fun(x)

  val empty: String => Boolean = _ == ""
  val notEmpty = neg(empty)
  println(notEmpty("foo") && !notEmpty(""))

  val valNeg: (String => Boolean) => (String => Boolean) = fun => (x: String) => !fun(x)

  val notEmptyVal = valNeg(empty)
  println(notEmptyVal("foo") && !notEmptyVal(""))

  def genericNeg[A](fun: A => Boolean): A => Boolean = !fun(_)

  val small: Double => Boolean = _ < 10
  val notSmall = genericNeg(small)
  println(notSmall(15) && !notSmall(5))
