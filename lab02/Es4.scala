package u02

// Exercise 4 and exercise 5
object Es4 extends App:
  def defCurriedCheck(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z

  val valCurriedCheck: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z

  def defCheck(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  val valCheck: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z