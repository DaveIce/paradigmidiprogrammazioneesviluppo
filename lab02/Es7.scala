package u02

// Exercise 7
object Es7 extends App:
  // Sum types model set union, so they are not good for modeling something like Rectangle >: Square
  enum Shape:
    case Rectangle(width: Double, height: Double)
    case Circle(radius: Double)
    case Square(side: Double)
    
  object Shape:
    def perimeter(shape: Shape): Double = shape match
      case Shape.Rectangle(w, h) => w * 2 + h * 2
      case Shape.Square(s) => s*4
      case Shape.Circle(r) => 2 * java.lang.Math.PI * r

    def area(shape: Shape): Double = shape match
      case Shape.Rectangle(w, h) => w * h
      case Shape.Square(s) => s * s
      case Shape.Circle(r) => java.lang.Math.PI * r * r

  import Shape.*

  println(perimeter(Shape.Square(5))) // 20
  println(perimeter(Shape.Rectangle(5, 10))) // 30
  println(perimeter(Shape.Circle(10))) // 62 e qualcosa

  println(area(Shape.Square(5))) // 25
  println(area(Shape.Rectangle(5, 10))) // 50
  println(area(Shape.Circle(10))) // 314 e qualcosa