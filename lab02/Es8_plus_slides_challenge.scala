package u02

// Exercise 8 + slides challenge
// Implement a method that takes an Optional[Int] and doubles its
// content (if not empty).
// Implement a method that takes an Optional[Boolean] and invert
// its value (if not empty).
// How would you write a generalisation of the two?
object Es8_plus_slides_challenge extends App:
  enum Option[A]:
    case Some(a: A)
    case None() // here parens are needed because of genericity

  object Option:
    def isEmpty[A](opt: Option[A]): Boolean = opt match
      case None() => true
      case _ => false

    def orElse[A, B >: A](opt: Option[A], orElse: B): B = opt match
      case Some(a) => a
      case _ => orElse

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match
      case Some(a) => f(a)
      case _ => None()

    // Exercise 8
    def filter[A](opt: Option[A])(p: A => Boolean): Option[A] = opt match
      case None() => None()
      case Some(a) => if(p(a)) Some(a) else None()

    def map[A, B](opt: Option[A])(p: A => B): Option[B] = opt match
      case None() => None()
      case Some(a) => Some(p(a))

    def combine[A, B](opt1: Option[A], opt2: Option[B]): Option[(A, B)] = (opt1, opt2) match
      case (Some(a), Some(b)) => Some((a, b))
      case _ => None()

    // Challenge: apply Generalisation + DRY + Strategy
    def apply[A](opt: Option[A])(p: A => A): Option[A] = opt match
      case None() => None()
      case Some(a) => Some(p(a))

    def double(opt: Option[Int]): Option[Int] = apply(opt)(_ * 2)

    def invert(opt: Option[Boolean]): Option[Boolean] = apply(opt)(!_)





  import Option.*

  val s1: Option[Int] = Some(1)
  val s2: Option[Int] = Some(2)
  val s3: Option[Int] = None()

  println(s1) // Some(1)
  println(orElse(s1, 0))
  println(orElse(s3, 0)) // 1,0
  println(flatMap(s1)(i => Some(i + 1))) // Some(2)
  println(flatMap(s1)(i => flatMap(s2)(j => Some(i + j)))) // Some(3)
  println(flatMap(s1)(i => flatMap(s3)(j => Some(i + j)))) // None
  // Filter
  println(filter(Some(5))(_ > 2)) // Some(5)
  println(filter(Some(5))(_ > 8)) // None
  println(filter(None[Int]())(_ > 2)) // None
  // Map
  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(Some(5))(_ > 8)) // Some(false)
  println(map(None[Int]())(_ > 2)) // None
  // Map2 (Combine)
  println(combine(Some(5), None())) // None
  println(combine(None(),Some(5))) // None
  println(combine(Some(5), Some(10))) // Some((5, 10))
  println(combine(Some(5), Some("test"))) // Some((5, test))