package u02

// Exercise 6
object Es6 extends App:
  def fibonacci(n: Int): Int = n match
    case n if n > 1 => fibonacci(n - 1) + fibonacci(n - 2)
    case _ => n

  println(fibonacci(2)) //1
  println(fibonacci(3)) //2
  println(fibonacci(10)) //55

  // Not tail recursive, checked with @annotation.tailrec